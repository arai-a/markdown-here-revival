/*
 * Copyright Adam Pritchard 2013
 * MIT License : http://adampritchard.mit-license.org/
 */

mocha.setup('bdd');
mocha.checkLeaks();
let expect = chai.expect;
